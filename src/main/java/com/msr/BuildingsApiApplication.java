

/******************************************
AUTHOR: MATTHEW SCHEID
To whom it concerns here is my submission.
I was able to implement loading the JSON data files into their own tables on the h2 Server, retrieving a Site and its
 supplementary info from the server by its id, and retrieving all Sites. Once setup I spend almost 2 hours working on
 the assignment.

 The main of the application class is hardcoded to use the maven org.JSON package to parse in the json objects from file
 into the h2 database. It then included a call to GetSites_ByID and GetSites, which implement the two requirements.
 I was not sure how this class would interact with a larger system, or how it was expected to function standalone,
 so I made the methods static so the functionality could be shown in the enclosed main.

 public static Triplet<Site,Integer,UseTypes> GetSite_ByID(int id, Statement h2_stm){
    I did not know how the site was expected to be returned, so I use the maven package javaTuples to bundle the
    Site, its total size, and its main useType together. I noticed that in the intro doc the return was represented as
    a JSON object, so with more time I would have created a toJSONString method for a subclass of Site that held these
    new fields(total size, prim type). It requires a Statement parameter to access the h2 server.

 public static List<Site> GetSites(Statement h2_stm){
    I figured a List<Sites> was a logical return. It was not specified if these would also be accompanied my total sizes
    or types so I excluded them, however they could easily be retained from the Get byId method. Again a statement was
    needed to talk to the h2 server.

*********************************************/
package com.msr;

import com.msr.model.Site;
import com.msr.model.SiteUses;
import com.msr.model.UseTypes;
import org.apache.tomcat.util.json.JSONParser;
import org.apache.tomcat.util.json.ParseException;
import org.javatuples.Pair;
import org.javatuples.Quintet;
import org.javatuples.Sextet;
import org.javatuples.Triplet;
import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
//import org.JSON.*;


import static java.sql.DriverManager.*;

@SpringBootApplication
public class BuildingsApiApplication {
    public static void main(String[] args) {
		SpringApplication.run(BuildingsApiApplication.class, args);

		String url = "jdbc:h2:mem:measurabl";
		String usr = "measurabl";
		String pas = "measurabl";

		try {
		    //H2 DATABASE CONNECTION
		    Connection conn = getConnection(url,usr,pas);
			Statement stm = conn.createStatement();
			stm.execute("DROP TABLE IF EXISTS SITES");
            stm.execute("DROP TABLE IF EXISTS SITE_USES");
            stm.execute("DROP TABLE IF EXISTS USE_TYPES");
			stm.execute("CREATE TABLE SITES(ID INT PRIMARY KEY,NAME VARCHAR(255),ADDRESS VARCHAR(255),CITY VARCHAR(255),STATE VARCHAR(255),ZIPCODE VARCHAR(255))");
			stm.execute("CREATE TABLE SITE_USES(DESCRIPTION VARCHAR(255),ID INT PRIMARY KEY,SITE_ID INT,SIZE_SQFT INT,USE_TYPE_ID INT)");
            stm.execute("CREATE TABLE USE_TYPES(ID INT PRIMARY KEY,NAME VARCHAR(255))");

			//JSON READING

            String Path = new File("").getAbsolutePath();

            FileReader sites_reader = new FileReader(Path.concat("\\src\\main\\resources\\data\\sites.json"));
            FileReader siteuses_reader = new FileReader(Path.concat("\\src\\main\\resources\\data\\site_uses.json"));
            FileReader usetype_reader = new FileReader(Path.concat("\\src\\main\\resources\\data\\use_types.json"));
			ArrayList site_lst = (ArrayList)new JSONParser(sites_reader).parse();
            ArrayList siteuse_lst = (ArrayList)new JSONParser(siteuses_reader).parse();
            ArrayList usetype_lst = (ArrayList)new JSONParser(usetype_reader).parse();

            //PARSE SITES.JSON INTO H2 DB
            for(int i = 0; i < site_lst.size(); i++){
                Object obj = site_lst.get(i);
                LinkedHashMap objmap = (LinkedHashMap) obj;
				String exeStr = String.format("INSERT INTO SITES (ID,NAME,ADDRESS,CITY,STATE,ZIPCODE) VALUES (%d,'%s','%s','%s','%s','%s')",
                        objmap.get("id"), (String)objmap.get("name"),(String)objmap.get("address"),(String)objmap.get("city"),
                        (String)objmap.get("state"),(String)objmap.get("zipcode"));
				stm.executeUpdate(exeStr);
			}
            //PARSE SITE_USES.JSON INTO H2 DB
            for(int i = 0; i < siteuse_lst.size(); i++){
                Object obj = siteuse_lst.get(i);
                LinkedHashMap objmap = (LinkedHashMap) obj;
                String exeStr = String.format("INSERT INTO SITE_USES (DESCRIPTION,ID,SITE_ID,SIZE_SQFT,USE_TYPE_ID) VALUES ('%s',%d,%d,%d,%d)",
                        objmap.get("description"),objmap.get("id"),objmap.get("site_id"),objmap.get("size_sqft"),
                        objmap.get("use_type_id"));
                stm.executeUpdate(exeStr);
            }
            //PARSE USE_TYPES.JSON INTO H2 DB
            for(int i = 0; i < usetype_lst.size(); i++){
                Object obj = usetype_lst.get(i);
                LinkedHashMap objmap = (LinkedHashMap) obj;
                String exeStr = String.format("INSERT INTO USE_TYPES (ID,NAME) VALUES (%d,'%s')", objmap.get("id"), objmap.get("name"));
                stm.executeUpdate(exeStr);
            }

            //Test if the functions work here
            System.out.println(GetSite_ByID(1,stm));
            try {
                System.out.println(GetSites(stm));
            } catch (Exception e){
                Logger lgr = Logger.getLogger(BuildingsApiApplication.class.getName());

                lgr.log(Level.SEVERE, e.getMessage(), e);
            }
        //exception handling
		}catch (SQLException ex) {
            ex.printStackTrace();
		}
		catch (FileNotFoundException ffe){
            ffe.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }




	/*GetSite_By_ID
        PARAMS:
            int id - the id associated with the site to retrieve
            Statement h2_stm -  a java.sql Statement created from an h2 Connection, used with .execute to commute with h2
        Returns:
            Triplet<Site,Integer,UseTypes> - This triplet contains the a site object created from the site held
                                in the h2 server corresponding to the id. It also contains that site's max sqft, and
                                its UseType with the largest sqft.
        Throws
    */
	public static Triplet<Site,Integer,UseTypes> GetSite_ByID(int id, Statement h2_stm) throws SQLException{

	    //FIRST query the h2 table for the site with this id
	    ResultSet site_RS =(ResultSet) h2_stm.executeQuery(String.format("SELECT * FROM SITES WHERE ID = %d",id));
        //null check on if it existed
        while(site_RS.next()) {
            Sextet<Integer,String,String,String,String,String> site = new Sextet<Integer,String,String,String,String,String>((Integer)site_RS.getInt("id"),
                    site_RS.getString("name"),site_RS.getString("address"),
                    site_RS.getString("city"),site_RS.getString("state"),
                    site_RS.getString("zipcode"));

            Site return_site; //site we are filling with the following info and returning
            ArrayList<SiteUses> site_use_list = new ArrayList<SiteUses>(); //one of the members of Site class, to fill in
            //this will be used to find the primary type of site use for the site. The map allows to track the total footage of a type
            int total_size = 0;
            HashMap<Integer,Integer> use_sqft_map = new HashMap<Integer,Integer>();
            int max_size = 0;
            int max_use_type_id = 0;
            UseTypes max_type = new UseTypes();

            //Now to go through site_uses to compile secondary info
            ResultSet uses_RS =(ResultSet) h2_stm.executeQuery(String.format("SELECT * FROM SITE_USES WHERE SITE_ID = %d",id));
            //want to create siteuse to add to list for site
            SiteUses cur_use = new SiteUses();
            //for every siteuse asociated with the site id
            //in the while loop add the data of each use to the array list. needed because the resultSet will close
            ArrayList<Quintet<String, Integer, Integer, Integer, Integer>> uses_List = new ArrayList<Quintet<String, Integer, Integer, Integer, Integer>>();
            while(uses_RS.next()) {
                uses_List.add(new Quintet(uses_RS.getString("description"),uses_RS.getInt("id")
                        ,id, uses_RS.getInt("size_sqft"),uses_RS.getInt("use_type_id")));
            }
            //now we can loop through the uses
            for(Quintet<String, Integer, Integer, Integer, Integer> sel_use : uses_List )   {
                //getting the info for the new site use object
                String desc = sel_use.getValue0();
                int use_id = sel_use.getValue1();
                int site_id = sel_use.getValue2();
                int size = sel_use.getValue3();
                int type = sel_use.getValue4();

                //now to grab the usetype info from the table, make it an object, and store that in a new siteuse object
                ResultSet type_RS =(ResultSet) h2_stm.executeQuery(String.format("SELECT * FROM USE_TYPES WHERE ID = %d",type));
                UseTypes cur_type = new UseTypes();
                while(type_RS.next()) {
                    cur_type = new UseTypes(type, type_RS.getString("name"));
                }

                //we can make the use obj now that we have the useType obj made
                cur_use = new SiteUses(use_id,site_id,desc,size,type,cur_type);

                //update the total sqft and manage the primary type polling
                total_size +=size;
                int newsize = size;
                if(use_sqft_map.containsKey(type)) {
                    newsize = use_sqft_map.get(type) + size;
                    use_sqft_map.replace(type, newsize);
                }
                else{
                    use_sqft_map.put(type, newsize);
                }
                if(newsize > max_size){
                    max_use_type_id = type;
                    max_size = newsize;
                    max_type = cur_type;
                }

                //add this site to the list, and move on to any more.
                site_use_list.add(cur_use);

            }
            //now we can create the site with all the parameters found and initialized
            return_site = new Site(site.getValue0(), site.getValue1(), site.getValue2(), site.getValue3(), site.getValue4(), site.getValue5(), site_use_list);
            return new Triplet<Site,Integer,UseTypes>(return_site, total_size, max_type);
        }

        //this is outside of the by id returns null check
        return new Triplet<Site,Integer,UseTypes>(new Site(), 0 , new UseTypes());
	}



    /*GetSites
        PARAMS:e
            Statement h2_stm -  a java.sql Statement created from an h2 Connection, used with .execute to commute with h2
        Returns:
            List<Site> - This ArrayList holds all Sites contained in the h2 server as Site objects
       Throws
    */
	public static List<Site> GetSites(Statement h2_stm) throws java.lang.Exception{
       //acts the same as above except query for all from SITES, adds a for loop outside for all the Sites, and
        //removes code for tracking max sqft and primary use type

        //FIRST query the h2 table for the site with this id
	    ResultSet site_RS =(ResultSet) h2_stm.executeQuery("SELECT * FROM SITES");
        //null check on if it existed
        ArrayList<Site> return_list = new ArrayList<Site>();
        ArrayList<Sextet<Integer,String,String,String,String,String>> site_tuple_list = new ArrayList<Sextet<Integer,String,String,String,String,String>>();
        while(site_RS.next()) {
            site_tuple_list.add(new Sextet<Integer, String, String, String, String, String>((Integer) site_RS.getInt("id"), site_RS.getString("name"), site_RS.getString("address"), site_RS.getString("city"), site_RS.getString("state"), site_RS.getString("zipcode")));
        }
        for(Sextet<Integer, String, String, String, String, String> site :  site_tuple_list  ){

            ArrayList<SiteUses> site_use_list = new ArrayList<SiteUses>(); //one of the members of Site class, to fill in
            //Now to go through site_uses to compile secondary info
            ResultSet uses_RS =(ResultSet) h2_stm.executeQuery(String.format("SELECT * FROM SITE_USES WHERE SITE_ID = %d",site.getValue0()));

            //want to create siteuse to add to list for site
            SiteUses cur_use = new SiteUses();
            //for every siteuse asociated with the site id
            ArrayList<Quintet<String, Integer, Integer, Integer, Integer>> uses_List = new ArrayList<Quintet<String, Integer, Integer, Integer, Integer>>();
            while(uses_RS.next()) {
                uses_List.add(new Quintet(uses_RS.getString("description"),uses_RS.getInt("id"),site.getValue0(),uses_RS.getInt("size_sqft"),uses_RS.getInt("use_type_id")));
            }
             for(Quintet<String, Integer, Integer, Integer, Integer> sel_use : uses_List )   {

                //getting the info for the new site use object
                String desc = sel_use.getValue0();
                int use_id = sel_use.getValue1();
                int site_id = sel_use.getValue2();
                int size = sel_use.getValue3();
                int type = sel_use.getValue4();

                //now to grab the usetype info from the table, make it an object, and store that in a new siteuse object
                ResultSet type_RS =(ResultSet) h2_stm.executeQuery(String.format("SELECT * FROM USE_TYPES WHERE ID = %d",type));
                UseTypes cur_type = new UseTypes();


                //Pair<Integer,String> type_list = new ArrayList<Pair<Integer,String>>();
                while(type_RS.next()) {
                    cur_type = new UseTypes(type, type_RS.getString("name"));
                }
                cur_use = new SiteUses(use_id,site_id,desc,size,type,cur_type);
                //add this site to the list, and move on to any more.
                site_use_list.add(cur_use);
            }
            //now we can create the site with all the parameters found and initialized
            return_list.add( new Site(site.getValue0(), site.getValue1(), site.getValue2(), site.getValue3(), site.getValue4(), site.getValue5(), site_use_list));
        }
        //this is outside of the by id null check
        return return_list;
	}


	/*
	public Site CreateSite(){
        Site s1 = new Site(   );

	}
	public Site UpdateSite(){

	}

	public Site DeleteSite(){

	}
*/



}
