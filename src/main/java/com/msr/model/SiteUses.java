package com.msr.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

/**
 * Site uses POJO
 *
 * @author Measurabl
 * @since 2019-06-11
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SiteUses {
    @Id
    private int id;

    private int siteId;

    private String description;

    private long sizeSqft;

    private int useTypeId;

    private UseTypes useTypes;
}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    