package com.msr.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Lookup types
 *
 * @author Measurabl
 * @since 2019-06-11
 */


@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class UseTypes {

    @Id
    private int id;

    private String name;
}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    